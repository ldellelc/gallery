{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Smooth case\n",
    "\n",
    "Consider the following optimal control problem (Lagrange cost, fixed final time):\n",
    "$$ \\int_0^1 |u|^2\\,\\mathrm{d}t \\to \\min, $$\n",
    "$$ \\dot{q} = v, $$\n",
    "$$ \\dot{v} = -\\lambda v^2 + u, $$\n",
    "with $q$ and $v$ fixed at $t=0$ and $t=1$:\n",
    "$$ q(0)=-1, v(0)=0, q(1)=0, v(1)=0. $$\n",
    "Denoting $x=(q, v)$, the Lagrange cost functional is defined by\n",
    "$$ f^0(t, x, u, \\lambda) = u^2, $$\n",
    "while the dynamics is\n",
    "$$ f(t, x, u, \\lambda) = (v, -\\lambda v^2+u). $$\n",
    "\n",
    "Denoting $p=(p_q,p_v)$, in the normal case ($p^0=-1/2$), the dynamic feedback is $u=p_v$, so the maximized Hamiltonian of the problem is\n",
    "$$ H(t, x, p, \\lambda) = p_v^2/2 + p_q v - \\lambda p_v v^2. $$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initializations\n",
    " \n",
    "To encode and solve the problem we use standard libraries such as `numpy` and `pyplot`, plus `nutopy`:\n",
    "\n",
    "* `nutopy.nle` is a Nonlinear Equation solver\n",
    "\n",
    "* `nutopy.tools` includes `@tensorize` and `@vectorize` decorators (see further)\n",
    "\n",
    "* `nutopy.ocp` defines some useful classes and methods to manipulate optimal control problems and related stuff (Hamiltonians, flows...)\n",
    "\n",
    "The initialization for the shooting method, an initial value for the unknown adjoint state $p_0$, is taken from `BOCOP` (also part of the\n",
    "[ct](https://ct.gitlabpages.inria.fr/gallery) project)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import time\n",
    "from nutopy import nle\n",
    "from nutopy.tools import *\n",
    "from nutopy.ocp import *\n",
    "\n",
    "t0 = 0.\n",
    "x0 = np.array([ -1., 0. ])\n",
    "lbda = 0.\n",
    "tf = 1.\n",
    "xf_fixed = np.array([ 0., 0. ]) # target is xf = (0, 0)\n",
    "p0 = np.array([ 12.00120012, 6.00060006 ] ) - np.array([ 6., 7. ]) # from BOCOP (+/- some perturbation)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hamiltonian\n",
    "\n",
    "The (normal and maximized) Hamiltonian is straightforwardly implemented, see `hfun`. The code requires that its first and second derivatives _wrt._ to state ($x$) and costate ($p$) are also provided. This derivatives, evaluated against first and second order increments are implemented by `dhfun` and `d2hfun`, respectively. These codes can be obtained through *automatic differentiation* for more complicated examples (see [here](https://ct.gitlabpages.inria.fr/gallery/kepler/kepler.html), _e.g._)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dhfun(t, x, dx, p, dp, lbda):\n",
    "    q, v = x\n",
    "    pq, pv = p\n",
    "    qd, vd = dx\n",
    "    pqd, pvd = dp\n",
    "    hd = 2*pv*pvd/2. + v*pqd + pq*vd - lbda*(v**2*pvd+pv*2*v*vd)\n",
    "    return hd\n",
    "    \n",
    "def d2hfun(t, x, dx, d2x, p, dp, d2p, lbda):\n",
    "    q, v = x\n",
    "    pq, pv = p\n",
    "    qd, vd = dx\n",
    "    pqd, pvd = dp\n",
    "    qd0, vd0 = d2x\n",
    "    pqd0, pvd0 = d2p\n",
    "    hdd = pvd*2*pvd0/2. + pqd*vd0 + vd*pqd0 - lbda*(pvd*2*v*vd0+vd*2*(v*pvd0+pv*vd0))\n",
    "    return hdd\n",
    "\n",
    "@tensorize(dhfun, d2hfun, tvars=(2, 3))\n",
    "def hfun(t, x, p, lbda):\n",
    "    q, v = x\n",
    "    pq, pv = p\n",
    "    h = pv**2/2. + pq*v - lbda*pv*v**2\n",
    "    return h\n",
    "\n",
    "h1 = Hamiltonian(hfun)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the use of the decorator `@tensorize` when defining `dhfun`, after having defined its first and second derivatives. Passing the _two_ positional arguments (`dhfun`, `d2hfun`) entails that `hfun` derivatives are provided up to order _two_, _wrt._ to variables specified by the named argument `tvars`: derivatives _wrt._ $x$ and $p$ (argument no. 2 and 3 of `hfun`, respectively). The decorated `hfun` function now contains its first and second derivative:\n",
    "\n",
    "* to evaluate $H$ at $(t_0,x_0,p_0,\\lambda)$,  type\n",
    "```python\n",
    "hfun(t0, x0, p0, lbda)\n",
    "```\n",
    "\n",
    "* to evaluate $\\partial_x H$ at the same point evaluated against vector $\\mathrm{d}x_0=(2.1,-3.2)$, type\n",
    "```python\n",
    "hfun(t0, (x0, dx0), p0, lbda)\n",
    "```\n",
    "\n",
    "* to evaluate $\\partial^2_{px} H$ at the same point evaluated against vectors $\\mathrm{d}x_0=(2.1,-3.2)$ and $\\mathrm{d}p_0=(3.1,-7)$,\n",
    "type\n",
    "```python\n",
    "hfun(t0, (x0, dx0, np.zeros(2)), (p0, np.zeros(2), dp0), lbda)\n",
    "```\n",
    "\n",
    "* and so on for further derivatives.\n",
    "\n",
    "Having defined the object `h1` of class `Hamiltonian` allows to evaluate $H$ (`h1.val`), its symplectic gradient $\\vec{H}$ (`h1.vec`), and [more](https://ct.gitlabpages.inria.fr/nutopy/api/ocp.html#nutopy.ocp.Hamiltonian)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.49940012003600176\n",
      "(0.49940012003600176, -19.203840384000003)\n",
      "(0.49940012003600176, -19.203840384000003, -9.920000000000002)\n",
      "0.49940012003600176\n",
      "[ 0.         -0.99939994  0.         -6.00120012]\n"
     ]
    }
   ],
   "source": [
    "dx0 = np.array([ 2.1, -3.2 ])\n",
    "dp0 = np.array([ 3.1, -7 ])\n",
    "print( hfun(t0, x0, p0, lbda) )\n",
    "print( hfun(t0, (x0, dx0), p0, lbda) )\n",
    "print( hfun(t0, (x0, dx0, np.zeros(2)), (p0, np.zeros(2), dp0), lbda) )\n",
    "print( h1.val(t0, x0, p0, lbda) )\n",
    "print( h1.vec(t0, x0, p0, lbda) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An alternative way of defining the maximized Hamiltonian is to set up an optimal control problem, that is an object of type [OCP](https://ct.gitlabpages.inria.fr/nutopy/api/ocp.html#nutopy.ocp.OCP), and then to use the class method [fromOCP](https://ct.gitlabpages.inria.fr/nutopy/api/ocp.html#nutopy.ocp.Hamiltonian.fromOCP). To set an `OCP`, the user needs to provide the function $f^0$ (Lagrange cost) and the dynamics $f$. These two functions are given below, as well as their derivatives up to order two. (Note the same use of `@tensorize` as before.) The user must also provide the _dynamic feedback_ that expresses $u$ as a function of $x$ and $p$, as a result of Hamiltonian maximization _wrt._ $u$. Here,\n",
    "$$ u(x,p) = p_v. $$\n",
    "In more complicated examples, there are several maximisers (including, _e.g._, bang and singular controls) and so several competing Hamiltonians to define using the same process. The expression `h2 = Hamiltonian.fromOCP(o, ufun, -0.5)` returns the (tensorized to order two) computation\n",
    "$$ H(t,x,p) = p^0 f^0(t,x,u(x,p))+\\langle p,f(t,x,u(x,p)) \\rangle $$\n",
    "with $p^0=-1/2$ (normal case). The resulting `Hamiltonian` object `h2` can be used exactly as the previously defined `h1`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def df0fun(t, x, dx, u, du, lbda):\n",
    "    df0 = 2.*u*du\n",
    "    return df0\n",
    "\n",
    "def d2f0fun(t, x, dx, d2x, u, du, d2u, lbda):\n",
    "    d2f0 = 2.*d2u*du\n",
    "    return d2f0\n",
    "\n",
    "@tensorize(df0fun, d2f0fun, tvars=(2, 3))\n",
    "def f0fun(t, x, u, lbda):\n",
    "    f0 = u**2   \n",
    "    return f0\n",
    "\n",
    "def dffun(t, x, dx, u, du, lbda):\n",
    "    q, v = x\n",
    "    dq, dv = dx\n",
    "    df = np.zeros(2)\n",
    "    df[0] = dv\n",
    "    df[1] = -lbda*2.*v*dv + du\n",
    "    return df\n",
    "\n",
    "def d2ffun(t, x, dx, d2x, u, du, d2u, lbda):\n",
    "    q, v = x\n",
    "    dq, dv = dx\n",
    "    d2q, d2v = d2x\n",
    "    d2f = np.zeros(2)\n",
    "    d2f[0] = 0\n",
    "    d2f[1] = -lbda*2.*d2v*dv\n",
    "    return d2f\n",
    "\n",
    "@tensorize(dffun, d2ffun, tvars=(2, 3))\n",
    "def ffun(t, x, u, lbda):\n",
    "    q, v = x\n",
    "    f = np.zeros(2)\n",
    "    f[0] = v\n",
    "    f[1] = -lbda*v**2 + u\n",
    "    return f\n",
    "\n",
    "o = OCP(f0fun, ffun)\n",
    "\n",
    "def dufun(t, x, dx, p, dp, lbda):\n",
    "    dpq, dpv = dp\n",
    "    du = dpv\n",
    "    return du\n",
    "\n",
    "def d2ufun(t, x, dx, d2x, p, dp, d2p, lbda):\n",
    "    d2pq, d2pv = d2p\n",
    "    d2u = 0.\n",
    "    return d2u\n",
    "\n",
    "@tensorize(dufun, d2ufun, tvars=(2, 3))\n",
    "def ufun(t, x, p, lbda):\n",
    "    pq, pv = p\n",
    "    u = pv\n",
    "    return u\n",
    "\n",
    "h2 = Hamiltonian.fromOCP(o, ufun, -0.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Shooting function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To define the shooting function, one must integrate the Hamiltonian system defined by `h1` (or equivalently `h2`). This is done by defining a [Flow](https://ct.gitlabpages.inria.fr/nutopy/api/ocp.html#nutopy.ocp.Flow) object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = Flow(h2) # h1 or h2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To compute the value of the Hamiltonan flow at time $t_f$ starting from time $t_0$ and initial conditions $(x_0,p_0)$ (and parameter $\\lambda$), do the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-2.49989999 -4.        ] [ 6.00120012 -7.00060006]\n"
     ]
    }
   ],
   "source": [
    "xf, pf = f.val(t0, x0, p0, tf, lbda)\n",
    "print(xf, pf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, `Flow.val` is tensorized, so you can compute the derivative of the flow, _e.g._, _wrt._ $p_0$ according to"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-4.01666667 -8.55      ] [  3.1 -10.1]\n"
     ]
    }
   ],
   "source": [
    "(xf, dxf), (pf, dpf) = f.val(t0, x0, (p0, dp0), tf, lbda)\n",
    "print(dxf, dpf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is now easy to write the shooting function, as well as its derivative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dshoot(p0, dp0):\n",
    "    (xf, dxf), _ = f.val(t0, x0, (p0, dp0), tf, lbda)\n",
    "    s = xf - xf_fixed # code duplication (in order to compute dxf, shooting also needs to compute xf; accordingly, full=True)\n",
    "    ds = dxf\n",
    "    return s, ds\n",
    "\n",
    "@tensorize(dshoot, full=True)\n",
    "def shoot(p0):\n",
    "    xf, _ = f.val(t0, x0, p0, tf, lbda)\n",
    "    s = xf - xf_fixed\n",
    "    return s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Nota bene.** In the case of a shooting function, you cannot evaluate the derivative of the flow without evaluating the flow itself. This is why the code of `dshoot` contains a duplication of the `shoot` computation. For performance, tensorization is done indicating `full=True`, to tell the kernel that `dshoot` indeed computes both the flow and its derivative. More on the [tensorize](https://ct.gitlabpages.inria.fr/nutopy/api/tools.html#nutopy.tools.tensorize) decorator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solve\n",
    "Finding a zero of the shooting function can be done using standard `SciPy` solvers, or by means of the [nle](https://ct.gitlabpages.inria.fr/nutopy/api/nle/nle-solve.html) solver of `nutopy` that is a _home made_ interface for the standard `hybrid` `Fortran` solver. The option `df=shoot` indicates that the function `shoot` is tensorized and can be used to obtain the derivative needed when `SolverMethod='hybrj'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "     Calls  |f(x)|                 |x|\n",
      " \n",
      "         1  4.716937561596717e+00  6.083847723304720e+00\n",
      "         2  1.910099915357094e-15  1.341640786499874e+01\n",
      "         3  1.570092458683775e-15  1.341640786499874e+01\n",
      "         4  4.453364592678439e-15  1.341640786499877e+01\n",
      "         5  1.831026719408895e-15  1.341640786499875e+01\n",
      "         6  8.950904182623619e-16  1.341640786499875e+01\n",
      "         7  3.662053438817790e-15  1.341640786499876e+01\n",
      "         8  5.950836838078908e-15  1.341640786499875e+01\n",
      "         9  0.000000000000000e+00  1.341640786499875e+01\n",
      "\n",
      " Results of the nle solver method:\n",
      "\n",
      " xsol    =  [12.  6.]\n",
      " f(xsol) =  [0. 0.]\n",
      " nfev    =  9\n",
      " njev    =  3\n",
      " status  =  1\n",
      " success =  True \n",
      "\n",
      " Successfully completed: relative error between two consecutive iterates is at most TolX.\n",
      "\n",
      "Elapsed time: 0.10782003402709961 \t p0_sol = [12.  6.] \t shoot = [0. 0.]\n"
     ]
    }
   ],
   "source": [
    "nleopt = nle.Options(SolverMethod='hybrj', Display='on', TolX=1e-8)\n",
    "et = time.time(); sol = nle.solve(shoot, p0, df=shoot, options=nleopt); p0_sol = sol.x; et = time.time() - et\n",
    "print('Elapsed time:', et, '\\t p0_sol =', p0_sol, '\\t shoot =', shoot(p0_sol))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plots\n",
    "The routine `Flow.val` is not only tensorized but also vectorized, _wrt._ to `tf`: when called with a `list` of final times, the `list` of corresponding values of the flow are returned (and can be directly used for plotting). For performance reasons, the vectorization is done iteratively: the flow is integrated from `t0` to `tf[0]`, then from `tf[0]` to `tf[1]` reusing the previous value as initial conditions, and so forth. More on the [vectorize](https://ct.gitlabpages.inria.fr/nutopy/api/tools.html#nutopy.tools.vectorize) decorator. (See also [here](https://ct.gitlabpages.inria.fr/nutopy/api/ocp.html#nutopy.ocp.Flow.val) for its more advanced use in `Flow.val`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "tags": [
     "nbsphinx-thumbnail"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.lines.Line2D at 0x7fb5b3cd0610>"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYQAAAEGCAYAAABlxeIAAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjMsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+AADFEAAAgAElEQVR4nO3deZxcZZXw8d+p3vdO791JOp21syeEsMgiYRUQZBFHQcFh0KgMjjOO78grjuKo8+I4o4wCAxEZxQUQUEBkFQkIhJAA2YB0Nsje6U5n632r8/7x3E5Vh4RUdWqv8/18nk9V3VvVfR7S3FPPc+89j6gqxhhjjC/eARhjjEkMlhCMMcYAlhCMMcZ4LCEYY4wBLCEYY4zxZMY7gGNRUVGhDQ0NYX+uqakJgMbGxghHZIwxie/111/fraqVh25P6oTQ0NDA8uXLw/7cggULAFi8eHFkAzLGmCQgIpsPt92mjIwxxgCWEIwxxniSesrImIhRhe690Lkbutpc694DPfsDra8Tetvd40AP9HfDQC/4+2GwD/yDoH6vKYgv0DIywZcFGdmQlQuZuZCVD9kFkFMI2UWQWxJo+eVeK4PCKvc+Y6LMEoJJfarQsQv2bXFt/1bYvx3ad8KBHW5fR4s7sB+O+CCnyB20cwrdgTwr3x2sM3MhI8sd7H2Z4PMSwNDvVQUdhMF+L3H0e4mkBzqaXXI5mGg6jtyHrAKXGIpqobgWiuugeAyUjoWSsTBqnEskxhwDSwgmdXS2we4m2L0e2tZD20bY8y7sfQ8Guoe/N7cEiurcwbVqmjvYFlZDfgUUeN/O80ZBbilkF7oDfbQNDkDvATdS6d7rRimdu6Gz1SWsjmZob4btb8DaP7mkEiyvDEY1QPlEKJ/kWsVkKJ8M2fnRj98kPUsIJvn0dUHL27BrDex6C3a9Da1roWt34D0ZOe7AWDYBJp3tDpSl47xv1GPcN/5Ek5HpRh35ZUd/r6pLGEOjnn2bXeLb8y5sXQqrHwKGCleKG0FUToPq6VA1HWpmuYThy4hih0yysYRgEltfFzSvhh1vwI43YedK2L3OzdOD+/ZeNQ2mXgiVU6Gi0X0rLhmT2gc7ESiocG30vPfv7++BPZvcf6vWJpcwW96B9c+4KSxw017VM6B2rvsZdcdBxZTU/u9mPpAlBJM4VN233K1LYdsy2PqaGwEMHcAKa6BuLkz7GNTOdt9yS+pjM52TbLJy3Wigevrw7QO9LkHsWgM7V7kEu/I+WPYztz+7CEYfB2NOgLEnwdgT3dSZSQuWEEz8qLpvru+95NqWV908OXgHpnlw2j/B6OPdt9fi2vjGmwoyc1wyrZ0Nc69y2/yD0LbBnZvY/jpsew1eujWQiKumQ/3J0HAajDsNiqrjF7+JKksIJrb2vAubFsO7L8C7fw3M+xePhvGnuwPP2JPdNJBNXcSGLwMqG12be6Xb1tfppui2LHGJetWDsPwet698Mkw4A8af4f7NbASRMiwhmOjq7XAH/w3Pwcbn3JQQuMsnJ53jDigNp7kTviJxDdUEyS5w/y4Np7nXgwPQvNKN5N79K6y4D5bd7S6xHX08TDzbnbwffbwl8iRmCcFEXttGWPeUO4G5+RV301ZWAYz/MJx8PUw40534tQSQPDIy3cF+9PFw6ldgoA+2L3ejvQ3PwYv/AS/c4i59nXQOTD4PJp9jo4ckYwnBHDu/310FtPZxWPuEuxcA3FU/J33BHRzGngyZ2fGN00ROZjaMO8W1M78BXXtg0/Ow/lnXVv8OJMPtb7wQpl0EpfXxjtochSUEMzL+Qdj8Mrz9mEsE7TvdnbrjToX5fweN57tr/016yC+DmR93ze93J6fXPQlNT8LT/9e12jkw7WKYfqkbIZqEYwnBhM7vhy2vwJrfwzt/hM4WyMxzUwNTL4YpH4G80nhHaeLN54OxJ7h29rfc/RDvPO7+Zv7yPdeqZsCMy2Dm5e4GQpMQLCGYD6bqrlVf/aBLBO07XBKY8hGYcambDrLCa+aDlE2AU//Btf3b4Z3H4K1H4PnvuVZ3HMy8AmZdAUU18Y42rVlCMIe3fxusegBW/c7dK+DLgsnnwszvQuMFlgTMyJSMhpO/5Nr+be5LxpqH4Jmb4Nl/hQkLYPYn3dSS/Y3FnCUEE9DX5c4HrPgNbHoBUKj/EFz0YzfvG0qNHWNCVTImMHLYvd77AvIA/OEL8Kd/dn9zc69yJ6btirSYSKiEICL3ABcBLao6M97xpI0dK+CNe920UO8BdzXIghvdN7Wy8fGOzqSDislw1jfhzJvczXArfgtv/QFW/BrKJsK8q2HOVXaXdJQlVEIAfgHcBtwb5zhSX2+HSwCv/687R5CZ62oEzbvalSew+kAmHkQCl7Ne8AN3Fdubv4I/3wzPedOV8//O3ctif6MRl1AJQVVfFJGGeMeR0lregdd+5s4N9LW7OjUX/BBmf8JuIjKJJbvAldKYe6WbUnrjXjedufZxd0nz8dfCvGtsKjOCEiohhEJEFgILAerr7UaXkAwOQNOfYOki2PySWytgxmXum9bYE21+1iS+islw3nfdtNI7f3R1lf78bXj+393VSScudJVwzTFJuoSgqouARQDz58/Xo7w9vXXtcd+qlt3tlo0sqYdzbobjrnGrghmTbDJzXAKYdYUrjb7s57DyfjdyqP+QuzN+6sWu1IYJm/1XS0VtG+HV/3H/k/R3QcPpbj52yvlWeMykjuoZcNGP4Jxvw5u/gdfuggf/1n3xOekL7nyYrTMdFksIqWTLUnj5v6HpCbfw+6y/cdd719gFWyaF5ZbAh653SWDdU7DkDndfw+Jb4PjPuoKKJaPjHWVSSKiEICL3AQuAChHZBnxbVX8e36gSnN8P6592C5psfdWdGP7w1+CEz9sleia9+DJg6kdd2/4GLLndjZSX3um+HJ36D26dDXNEYScEETkX+BvgdlVdISILvXn9Y6aqV0bi56SFwQF46/fw0o/dgvMl9XDBf8Bxn7E7PI0ZPQ+u+LmrpfTqHe5c2srfwtSL4LSvwpjj4x1hQhrJCOF64FrgmyJSBtip/Vga6HNr4L70I7fYTOVUuGyRKxKWkRXv6IxJLKPGufNnH/4Xd45h6V3ustUJC9y2hlPjHWFCGUlCaFXVfcDXROQW4IQIx2QOZ6DX3aDz0q3uiqG64+C877ta83aDjjEfrKDcrdtwypfdJauv3Aa/uNCVaz/j627xJrv8ekQJ4U9DT1T1RhH5cgTjMYca6HOJ4K8/ggPbYMyJcNGtbrlC+wM2Jjw5RW7FtxMXwuu/hJdvhXs/5hLDghtdYkhjIX+1FJFbRURU9dHg7ar608iHZRgccPOeP50Hf/oqFNfB1X+A655x6w9YMjBm5LLy4OQvwj+scHfq79kEv7wYfnERbHk13tHFTThzDR3AYyKSDyAi54nIy9EJK435/bDqQbj9BHjsy1BYBZ952CWCiWdZIjAmkrJy4aSFLjGc/wNobYJ7PgK/vsIVfUwzIU8Zqeo3ReQq4AUR6QU6gRujFlm6UXVr0T73Hdi1Bqpnwqfuc8W8LAkYE11ZuW7EMO9qV+vr5Vth0RmuxMuZ34SKSfGOMCZCTggicjbweVwiqAWuU9WmaAWWVrYug2e/5ZanHNUAH/85zLjcThYbE2vZBXDaP8L8a92J5yW3u4qr865x5xhSfEW3cI44NwH/qqoLgCuAB0TkrKhElS7aNsLvroGfnwNtG+DC/4S/X+bqtFgyMCZ+ckvgrJvgKyvghOvchR0/Oc4V0+ttj3d0URPOlNFZQc9Xi8gFwMPAKdEILKV17XG31S//uas8uuAb8KG/h5zCeEdmjAlWWAUX/hBO+iL85bvwwg9g+f+6S1iPuzrliuiN+Guoqu4Ezo5gLKlvoA9e+Sn8ZC4s+5n7g/qHN2HB1y0ZGJPIyifCJ34Bn/uLe/74P8Kdp8GGP8c7sog6pnkJVe2OVCApTRXWPgF3nATPfNPdS/ClV+DiW63ekDHJZMzxcO2T8Df3wkAP/Prj8JtPuAV8UoBNVEdbyzvwq0vh/ivBlwWffhg+85AV2TImWYnA9Evg75fCud919y3ccTI89X+he1+8ozsmlhCipXsfPHkj/M+psONNV3juSy+7m8qMMckvM8dVUP3yGzD3066y6m3z4Y1fufuJktBRz4iIyFc/aL+q/ihy4aQAv98Vn3v2W9DVBsf/LZz1r7ZCmTGpqrASPvYTtyTtk/8Cj93g6iV99L9c1dUkEsoIochr84EvAaO99kVgevRCS0I7V8H/ng+PXg9lE2DhYneewJKBMamvbi783dOu+vCB7fCzs+Dxf3JXFSaJo44QVPU7ACLyDDBPVdu91zcDD0Y1umTR2+6uT156p1ug5pLbYc5Vdi+BMelGBOZ8EhrPd5eWL70L3n4UzvsezLky4asOhHPEqgf6gl73AQ0RjSbZqMJbj8BtJ7r5w3mfhS+/7hapsWRgTPrKLYHz/x984QUonwSPfAl+8VFoWRvvyD5QOHdV/Ap4TUT+4L2+FLg38iEliX1b4E9fc8tX1syCT/4KxsyPd1TGmERSMwuufcrd6fzst9y9C6f9I5z+NVc/KcGEc6fy90XkSeB0QIFrVfXNqEWWqAYH3MpLf/mee33e991djCl2x6IxJkJ8Pjj+s26t56dvghd/CGt+Dxf9GCacEe/ohglnPYQcYCpQAJQCF4vIt6IVWEJqXuPqDj39DWg43V2HfMoNlgyMMUdXUAGX3wVXPwLqdwvzPHoDdO+Nd2QHhTPR/ShwCTCAq3g61FLfQK8bESw6A/ZthSvugasegNL6eEdmjEk2E8+E65fAqf8IK34Lt58E7/wx3lEB4Z1DGKOq50ctkkS1dRk8+vewu8ldJfCRf4f8snhHZYxJZll5cO533HoLj90AD3wGpl/qKh4XVsYtrHBGCK+IyKyoRZJo+rvdfN8950Ffpys5cdmdlgyMMZFTNxc+/zyc/S1oegJuPxFWP+SuYIyDcBLCacDrItIkIqtEZLWIrIpWYHG19TV3NcCS29ylpNcvsZITxpjoyMiC0/8ZvvBXd0Prw9fB766GjtaYhxLOlNEFUYsiUfT3wOJ/dyWqi0fDNY/ChAXxjsoYkw6qprq101/5KTz/fdh8kit/MeOymIUQ8ghBVTcDB4BqYFxQSw07VsCiBfDyf7vl8r70iiUDY0xs+TLcfQpf+Ku7aOXBv4WHrotZ+Ytw1lT+HPAVYAywAjgZWAIk9zKagwPw0o/hhVsgv8KdK7DpIWNMPFVNhev+HDg2bX4ZLrkNJkX32BTOOYSvACcAm1X1TOA4IKKTXCJyvneOYoOI3BjJn31YbRtdMbrnv+fqm9u5AmNMosjIhDP+D3zuOVcK49cfhz/9M/R1Re1XhpMQelS1B9xNaqq6FmiMVCAikgHcjjtXMR24UkSiV021vRnuPB12r4OP/9zdW2BXEBljEk3dXFj4Apx8PSy7G+76sFtjJQrCOam8TURKgUeAZ0VkL7AjgrGcCGxQ1U0AInI/7ka4t4/0gaamJhYsWBD2L1qxbAkM9rGgswwqxsFTtwG3jSxqY0zaUoVBVQYHlQH1M+jXYc2veI968NHvxz2q2+9XRRXUe60EXiuBK1AVKKacibKGrP86ka7SyRSUVkW0P+HUMho61X2ziDwPlABPRTCW0cDWoNfbgJMOfZOILAQWAuTk5IzsN2VkufoiNelzW4Ux5oP5VekfVPoH/QwMPfqVgUE//d6je60M+t3zQX9o9wv4RPD5hAwJPPeJ4BPI9PkQAfFeC+K9DjwHcA+5tOooSnp3kJlXGvH/BiMqwqOqL0Q6EIb6e8ivOszvXgQsApg/f74uXrw47F80NKoYyWeNMclj0K+0dfSy60Avuw700NrRS8uBXlo7emht76W1vZfdHX20dfTS2Td42J+R4ROq8rMozc9mVH4WJXnZlORlUZyXSXFulvc8i8KcTIpzMynMzaQgJ5PCHPeYl5VBhi+x1kGQI6zLkEhV2bYBY4NejyGyU1LGmBTSP+hn14EeduzrYef+bnbu76F5v3vevL+H5gPuoH+4L/Gj8rOoLMqhojCH4+pLKS/IoaIom4qCHMoKsikrzKYs3z0W5WQe8QCaahIpISwDJovIeGA78CngqviGZIyJl+6+Qbbt7WLbvm627e1m+95utu/rZsc+97ylved9B/uinExqSnKpKcllSnURNSW5VBXnUlWUQ7X3WFGYQ3amLWB1OAmTEFR1QERuAJ4GMoB7VPWtOIdljImSQb/SfKCHzW2dbN3TxZY9XWzZ083WPV1s29vF7o6+Ye/PyhDqSvOoK8nj1EkVjB6Vx+jSXGpL8qgrzaW6OJei3Kw49SY1HHNCEJGvq+oPIhGMqj4BPBGJn2WMib++AT/b9naxua2L99o6Dz5uaeti295u+gb9B9+b6XMH/LFleZwzrZqxZfmMGZXntXwqC3PwJdhcfKoJOyGIyO+CXwJzgYgkBGNM8hn0Kzv2dfPu7s6DbdPuTt7b3cn2fd3DrsQpyM5gXHkBU2uLOG9GDePK86kvc622JJfMDJvKiaeRjBAOqOrnhl6IyP9EMB5jTILa393PptYONrZ2sqm1g02tXgJo66RvIPBNvyA7g4aKAmaPKeFjc+poqChgfEU+48oLKC/ITpsTtMloJAnh+4e8vikSgRhj4k9V2bm/hw0tHWxo6WBj61DrpLW99+D7Mn1CfVk+EyoLOKOxkvEVBYyvKGBCZQGVhTl20E9S4RS3uxX4J1V9N3i7qsamDJ8xJmIG/cq2vV2s39XB+pYO1re0uwTQ0jHsevzi3EwmVRVyxpRKJlUVMrGykAmVBdSX5ZNl0zspJ5wRQgfwmIh8SlU7ReQ84NuqemqUYjPGHKNBv7JlTxfrdrkD/rpd7azf5b719wZN89QU5zKpqpBPzB/LxKpCJlUWMrHKvu2nm3BKV3xTRK4CFotIL9AJRL8iqTHmqPx+Zfu+bpqa21nX0s665nbWHebAP7o0j0lVhZwysZwp1UVMqi5kUlUhxXa5piG8KaOzgc/jEkEtcJ2qNkUrMGPM+6kqre29NO1qdwf/Xe007epg/a52uoKmeupKcplcXcRpkyuYVFXoDv5VhRTmJMytRyYBhfPXcRPwr6r6kojMAh4Qka+q6l+iFJsxae1ATz/rmtsPHvybvOf7uvoPvqeiMIcp1YV88oSxNFYXMbm6iMnV9o3fjEw4U0ZnBT1fLSIXAA8Dp0QjMGPSRd+An42tHTQ1t7O2uZ2m5gOs29XB9n3dB99TmJPJlOpCLphZS2N1IVNqimisLqK8cIQVf405jBGPH1V1pzeNZIwJgWpgnn9t0MF/U2snA97NW1kZwsTKQuY3jOLTNfU0VhfRWFPE6NI8O7lrou6YJhRVtfvo7zIm/Rzo6Q8c+HceODjd094zcPA9o0vzmFpTxDnTqmmsKWJqTTHjKwqs8JqJGzvDZMwx6B/0s6m1k7XNB7xv/K4FT/cU5WYytaaIS+eOprGmiGm1RUypLrJCbCbhWEIwJgRDd/AGz/OvbW5nY2sH/YNuuifTN3y6Z6r3rb+2JNeme0xSsIRgzCH2d/ezblfQCd7mDtY2H+BA0HRPXUkujTVFLGiscgf+2iImVBTadI9JapYQTNrq6R88eHVP0y53M1dTczs79vccfE9RTiaNNUVcPKfOO/AXM6W6iJI8m+4xqccSgkl5A4N+Nu/pOnhN/zrvuv732roOlmYeurrnhPFl3gneIhpriqmz6R6TRiwhmJTh9yvb9nYfPOiv9+7i3djacbA8swiMK8tncnURF86qpdG7nr+hosCKtZm0ZwnBJJ2hA//6FlevZ32LK9i2oaWD7v73l284fXIFU6qLmFJdyOSqIvKyM+IYvTGJyxKCSVj9g342t3V5tfldtc71Xo3+nv5Awbbq4hymVBdx5Yn17qBv5RuMGRFLCCbuOnsH2NTaycbWjoMLs2xo7WBzW+fBSzrBfeOfVF3EyRPKvYJthUyqshO8xkSKJQQTE0PlmTftDiy/uGm3e9wZdFVPhk8YV5bPxKpCzp1ezaTKQiZXu4VZCqxSpzFRZf+HmYhRVVo7enlvdxfveWvtvnuEdXeLcjOZUFnIhyaWM7GykImVBUyqKqS+zEo3GBMvlhBMWPx+ZVd7D+/t7mLLnk42t3Wxua2L99rc847ewM1bWRnC2LJ8xpcX8OEpFUyoLLR1d41JYJYQzPu09/SzdU83W/d2sXWPa1u8tnVv97Bv+pk+d9AfV57PCQ1lNJTn01BRQEN5AWNG5ZFpl3IakzQsIaQZVWVfVz/b93WzfV83O/Z1s31vN9v2drNtXxdb93Szv7t/2GcKczIZW5bP5Koizp5WzdiyfHfgLy+gtiTXDvrGpAhLCClEVdnT2UfzgR52Hehh5/4edu13jzv397Bjfzc79/UMu1YfIDfLx5hR+YwZlcecMaWMLctnrPe6viyf0vwsm94xJg1YQkgCPf2D7O7opa2jj90dvbS2u9bS3ktLe497POCeB1+mCeATqCrKpaYkl6k1RZzVWEVtaR6jS3MZXZpPXWkuZQXZdsA3xiRGQhCRTwA3A9OAE1V1eXwjig5VpbNvkP3d/ezv6neP3X3s7epnX1c/e7v62NvZx96uPto6+2jr6GNPZ9+wE7XBSvKyqCrKoao4h5PGl1FVnEt1cQ41xblUl+RSU5xLZVGOlWQwxoQkIRICsAa4HLgrFr/Mr4qqO3mqgPrdtkFV/H5lwK8M+pX+QT/9g+6xd8BP78AgfQPueU//ID39g3T3DdLlPXb2DtLVN0BH7wBdfYN09AzQ3jtAe08/7T3u0a9Hjisn00dZQTal+dmUF2Qzdmw+ZQXZVBblUFGYTXlBjnvuvc7JtBIMxpjISYiEoKrvADGbttjc1sWuAz3MuvmZiP1MEcjPyqAgJ5OCnEzyszMoys1kdGkehTmFFOdlUZybRVFuJiV5WZTmZ1Gcl0VpXjal+e51XlaGTd0YY+ImIRJCOERkIbAQoL6+fkQ/o6wgm9wsH1+7cBoiLhFlCPh8gk+ErAwhw+cj0ydkZ/rIyvCRlSHkZGaQk+UjJ9NHblaGa5k+CnIyycn02cHcGJPUYpYQROTPQM1hdt2kqo+G+nNUdRGwCGD+/PkfMAFzZCV5WZTkZfH5D08YyceNMSYlxSwhqOo5sfpdxhhjwmeXnxhjjAFAVEc06xLZIEQuA34KVAL7gBWq+pEQPtcKbB7hr60Ado/ws8nK+pwerM/p4Vj6PE5VKw/dmBAJIR5EZLmqzo93HLFkfU4P1uf0EI0+25SRMcYYwBKCMcYYTzonhEXxDiAOrM/pwfqcHiLe57Q9h2CMMWa4dB4hGGOMCWIJwRhjDJAGCUFEzheRJhHZICI3Hma/iMhPvP2rRGRePOKMpBD6/Gmvr6tE5BURmROPOCPpaH0Oet8JIjIoIlfEMr5IC6W/IrJARFaIyFsi8kKsY4y0EP6uS0TkjyKy0uvztfGIM5JE5B4RaRGRNUfYH9njl6qmbAMygI3ABCAbWAlMP+Q9FwJPAgKcDCyNd9wx6PMpwCjv+QXp0Oeg9/0FeAK4It5xR/nfuBR4G6j3XlfFO+4Y9PkbwA+855XAHiA73rEfY78/DMwD1hxhf0SPX6k+QjgR2KCqm1S1D7gfuOSQ91wC3KvOq0CpiNTGOtAIOmqfVfUVVd3rvXwVGBPjGCMtlH9ngC8DDwMtsQwuCkLp71XA71V1C4CqpkOfFSgSV3a4EJcQDr+6VJJQ1Rdx/TiSiB6/Uj0hjAa2Br3e5m0L9z3JJNz+XIf7hpHMjtpnERkNXAbcGcO4oiWUf+MpwCgRWSwir4vINTGLLjpC6fNtuFUXdwCrga+oqj824cVNRI9fSbceQpgOt0DBodfZhvKeZBJyf0TkTFxCOC2qEUVfKH2+Ffi6qg6mwLoVofQ3EzgeOBvIA5aIyKuqui7awUVJKH3+CLACOAuYCDwrIn9V1QPRDi6OInr8SvWEsA0YG/R6DO7bQ7jvSSYh9UdEZgN3AxeoaluMYouWUPo8H7jfSwYVwIUiMqCqj8QmxIgK9e96t6p2Ap0i8iIwB0jWhBBKn68FblE3ub5BRN4FpgKvxSbEuIjo8SvVp4yWAZNFZLyIZAOfAh475D2PAdd4Z+tPBvar6s5YBxpBR+2ziNQDvweuTuJvjMGO2mdVHa+qDaraADwEXJ+kyQBC+7t+FDhdRDJFJB84CXgnxnFGUih93oIbESEi1UAjsCmmUcZeRI9fKT1CUNUBEbkBeBp3lcI9qvqWiHzR238n7oqTC4ENQBfuW0bSCrHP3wLKgTu8b8wDmsSVIkPsc8oIpb+q+o6IPAWsAvzA3ap62EsXk0GI/8bfBX4hIqtxUylfV9WkLoktIvcBC4AKEdkGfBvIgugcv6x0hTHGGCD1p4yMMcaEyBKCMcYYwBKCMcYYT1KfVK6oqNCGhoawP9fU1ARAY2NjhCMyxpjE9/rrr+/Ww6ypnNQJoaGhgeXLl4f9uQULFgCwePHiyAZkjDFJQEQ2H267TRkZY4wBknyEMFLvtXXS1TvIJ+9aEu9QjDFmRKbXFfPti2dE9GfaCMEYYwyQpiOEhvICAB74wofiHIkxxiQOGyEYY4wBLCEYY4zxWEIwxhgDWEIwxhjjsYRgjDEGiENCEJF7RKRFRNYEbSsTkWdFZL33OCrWcRljTLqLxwjhF8D5h2y7EXhOVScDz3mvjTHGxFDME4KqvgjsOWTzJcAvvee/BC6NaVDGGGMS5hxC9dA6oN5j1ZHeKCILRWS5iCxvbW2NWYDGGJPqEiUhhExVF6nqfFWdX1n5vuqtxhhjRihREsIuEakF8B5b4hyPMcaknURJCI8Bn/WefxZ4NI6xGGNMWorHZaf3AUuARhHZJiLXAbcA54rIeuBc77UxxpgYinm1U1W98gi7zo5pIMYYY4ZJlCkjY4wxcWYJwRhjDGAJwRhjjMcSgjHGGMASgjHGGI8lBGOMMYAlBGOMMR5LCMYYY4ARJAQROVdEfiYic73XCyMfljHGmFgbyZ3K1wPXAt8UkTJgbmRDMsYYEw8jmTJqVdV9qvo14DzghAjHZIwxJg5GkhAeHwVpxEwAABJqSURBVHqiqjcC90YuHGOMMfESckIQkQtEZCnwnyLyOxH5EICq/jRq0RljjImZcEYIdwBfBU4CFgE/FJEjVS5NbL3t0L0H2pvjHYkxxiSMcE4q71LVl73nfxaRJcBS4L7IhxVl7c3QsQv+qxEKa6B2jmt1c91j8WgQiXeUxhgTU+EkhPdE5HvAv6lqH9APtEcnrCgrmwCF1XD+Qti5AnauhA3Pgvrd/vxyqJ07PFGUjrMkYYxJaeEkBAUuBz7vrWxWD/xGRCar6vpIBCMi7+GSzCAwoKrzI/Fz38eXAbnFcPIXA9v6umDXGpccdq6AHSvhlZ+Af8Dtzy3xEsTcwGPZBPDZvX3GmNQQckIYWulMRHKBmcAcr90tIhNUdWyEYjpTVXdH6GeFLjsfxp7o2pD+Hmh520sQK6B5FSy9Ewb7vM8UQe3sQJKomwvlk1zCMcaYJBP2jWmq2gMs91pqy8qF0fNcGzLQB61rg0YSK2D5z2Ggx/tMPtTMGj6aqJwKGTFfrdQYY8KSaEcpBZ4REQXuUtVFh77BK5WxEKC+vj7G4QGZ2d6oYDZwtds2OAC713nnI1a5xzd/A6954WfmQvWM4eclqqa7n2WMMQki0RLCqaq6Q0SqgGdFZK2qvhj8Bi9JLAKYP3++xiPI98nIhOrprs29ym3zD0LbxsBIYudKWP2gG00A+LLc+4PPSVTPcKMSY4yJg4RKCKq6w3tsEZE/ACcCL37wpxKULwMqp7g2+xNum98Pe9/1koSXKN55DN74pdsvGVA1bfh0U81MyC6IXz+MMWnjqAlBRL76QftV9UeRCERECgCfqrZ7z88D/i0SPzth+HxQPtG1mZe7baqwb0tgFLFzJax7Glb8xu0XH1RMGT7dVDPLXSVljDERFMoIoch7bMQVsnvMe30xkf32Xg38Qdy1/pnAb1X1qQj+/MQkAqPGuTb9ErdNFdp3uhPWQ4ni3Rdg1f2Bz5VPOuQy2DmQVxqfPhhjUsJRE4KqfgdARJ4B5qlqu/f6ZuDBSAWiqptwl7EaESiuc23qhYHt7buGTzdtfQ3WPBzYP6rh/fdKFJTHPHxjTHIK5xxCPdAX9LoPaIhoNOaDFVVD0Xkw5bzAts62oOkm7zLYtx8N7C8eEyjJMZQoiqpjH7sxJuGFkxB+BbzmnewFuBQrfR1/BeUw6WzXhnTvDVz+OvS49vHA/qLawDTTUKIorrPSHMakuXDuVP6+iDwJnI67X+BaVX0zapGZkcsbBRPOcG1IzwFXmiP4vMT6Z4LqN1W8fyRRWm9Jwpg0EnJCEJEcYCpQ4H3uYhG5WFVT60qgVJVbDONOcW1IXyc0rwk6L7ESNt4KOuj25416/0hi1Hir32RMigpnyuhRYD/wOtAbnXBMTGUXQP1Jrg3p74GWt4aPJJbcAf5+tz+nxLtTOyhJlE+0+k3GpIBwEsIYVT0/apGYxJCVC6OPd23IQJ8r8te8KpAolt0dVL+pwN0bETzlVDHF6jcZk2TC+T/2FRGZpaqroxaNSUyZ2e5gXzcX5l3jtg32u/pNO4JuqHvjXujv8j6T5+6yDh5JVE61+k3GJLBwEsJpwN+KyLu4KSMBVFVnRyUyk9gyslztpeoZcNyn3Tb/ILRtGD7dtPIBN5oAyMh2Rf0Ork431722+k3GJIRwEsIFUYvCpAZfBlQ2ujbnk26b3w97NkHzykCiePuRQP0mXyZUToO6oKubqme69SmMMTEVzmWnm0VkFDAZCP5KtzniUZnU4fNBxSTXZn7cbVOFfZuHTzc1PQlv/trtFx9UNA4fSdTMhJyiI/8eY8wxC+ey088BXwHGACuAk4ElwFnRCc2kLBFXZmNUA8y41G1ThQPbXXIYShSbng+q3ySB+k1DSaJ2tlva1BgTEeFMGX0FV9zuVVU9U0SmAt+JTlgm7YhAyRjXpn40sP3AzuFXN215FdY8FNg/anzQ1U3etFN+WezjNyYFhJMQelS1R0QQkRxVXSsijVGLzBiA4lrXpnwksK2j1Y0ghs5LbH8d3vpDYH9JvRs91M2F2uNcoiisjH3sxiSZcBLCNhEpBR7BrWa2F9gRnbCM+QCFlTD5HNeGdO0JGkl45yWG1W+qC5pu8kYTRbVWmsOYIOGcVL7Me3qziDwPlACpv16BSQ75ZTBhgWtDevZD8+rh5yXWPYUrxQUUVB2SJOa6KStLEiZNjehWUlV9IdKBGBNxuSXQcJprQ3o7XJJoXhVIFBv/ElS/qSwwghhKFKPGW5IwaSGhaguIyPnAfwMZwN2qekucQzKpJqcQxn3ItSH93bDrLdjxZmC6acnt76/fdPDqpjlQNtGK/JmUkzAJQUQygNuBc4FtwDIReUxV345vZCblZeXBmPmuDRnohZZ3Andc71gBSxfBoFfXMbsQamYPH0lUTLEifyapJUxCAE4ENnhLaSIi9wOXAEdMCE1NTSxYsCDsX7RixQqAEX3WpDGd5mo19XW41vsq9D0XWFNCfK6CbHahG4lkF0JWvk03maRxzAlBRL6uqj+IQCyjga1Br7cBJx36JhFZCCwEyMnJicCvNSZEIt4BvwAYWoZU3ZRTb0cgUXS2QPtO7zM+V4YjuzCQKLLy3XZjEkzYCUFEfhf8EpgLRCIhHO5rlL5vg+oiYBHA/PnzdfHixWH/oqGRwUg+a8xR+f2wZ+Pwda6bV0GPlyR8WVA1Lejk9XGuSGBWXnzjNmlDjjBqHckI4YCqfi7oB//PSIM6xDZgbNDrMdh9DiYZ+XxQMdm1WVe4baqw973AOYmdK2Htn+DNX7n94hUGHDppXTfXFfnLKYxbN0z6GUlC+N4hr2+KRCDAMmCyiIwHtgOfAq6K0M82Jr5EoGy8azO8W3pUYf+24SOJDc/Cyt8OfcidqB62jKnVbzLRE05xuwuAm4FSEVkJ/FhVl6jqnkgEoqoDInID8DTustN7VPWtSPxsYxKSCJSOdW3aRW6bqjv/MDSK2LEC3nsJVgfN1JZNGD6SqJlt9ZtMRIQzQrgD+Azuqp/jgR+KyO2qel+kglHVJ4AnIvXzjEk6IlBc51pj0BIkHS2BkcTOlbBtObz1+8D+0vrA3da13up2BRWxj98ktXASwi5Vfdl7/mcRWQIsBSKWEIwxR1BYBZPPdW1I157hSWLnSnjnj4H9xaMDI4mh0URRTexjN0kjnITwnoh8D/g3Ve0D+oH26IRljDmq/DKYeKZrQ7r3efWbVgTqNzU9wcEL9gqrh0831c5xicPulTCElxAUuBz4vIisB+qB34jIZFVdH5XojDHhySuF8ae7NqS33UsSq4afvB66oS6/PGi6yUsUpeMsSaShcKqdXgkgIrnATGCO1+4WkQmqOvaDPm+MiZOcIhh3imtD+rpc/aadKwJTTq/8BPwDbn9u6fCrm+qOc0X+rH5TSgv7slNV7QGWe80Yk4yy82HsCa4N6e+BlreHn5dYeicM9nmfKXKXvQ6NJurmumVNrX5TykikWkbGmHjKyoXR81wbMtAHre8Mn25a/r8w0O19Jh9qZg0/eV05FTLs0JKM7F/NGHNkmdmBAz1Xu22DA7B73fCRxJu/htfu8j6T60pxBI8kKqe5n2USmiUEY0x4MjKherprc6902/yD0LYhMJLYuRJWPwTL73H7fVnu/cH3SlRPt/pNCcYSgjHm2Pm8WkyVjTD7E26b3w/73gta53oFvP0YvHGv2y8ZQUX+vCmnmpleNVkTD5YQjDHR4fO5MhtlE2Dm5W6bKuzbEjTdtArWPwMrfuP2i++Q+k1z3TmK3OL49SONWEIwxsSOCIwa59r0j7ltqnBgR+Bu650r4N0XYdUDgc+VTxo+kqidDXmj4tOHFGYJwRgTXyJQMtq1qRcGtrfvGp4ktr4Gax4O7B/VcEgl2OOgoDzm4acSSwjGmMRUVA1F58GU8wLbOtuCbqbzTmC//Whgf8nY4dNNtXPczzEhsYRgjEkeBeUw6WzXhnTv9ZJD0GWwax8P7C+sCdRtGkoUxXVWmuMwLCEYY5Jb3iiYcIZrQ3oOePWbgqac1j8TqN9UUHnIdNNcV0I8zZOEJQRjTOrJLYaGU10b0tcJzWuGTzdtfB500PtM6fAqsLVz065+U0IkBBG5Gfg80Opt+oa3WI4xxkRGdgHUn+TakP4eaHkrsDrdzpWw5A7w97v9OcXvH0mUT0rZJJEQCcHzY1X9z3gHYYxJI1m5MPp414YM9AUV+fOmm5bdDQM9bn92oVe/KShJVExJifpNyd8DY4yJpMxsN21UNzewbbDf1W86eNf1SnfHdX+X95lcqJ45fLqpcmrS1W9KpIRwg4hcgyur/c+quvdwbxKRhcBCgPr6+hiGZ4xJWxlZrmBf9Qw47tNu28H6TUHTTSsfcKMJgIzsoCJ/Xqua4UYlCUpUNTa/SOTPwOEWdL0JeBXYjVuV7btArar+3dF+5vz583X58vCXZViwYAEAixcvDvuzxhhzRH4/7H0Xdrw5fMqpZ7/b78t0lV/rgov8zXDrU8SQiLyuqvMP3R6zEYKqnhPK+0TkZ8DjR32jMcYkGp8Pyie6NusKt00V9m0ePpJoetKVDAevflPj8Cucama5le5iLCGmjESkVlV3ei8vA9bEMx5jjIkYEVdmY1QDTL/EbVOFA9uDksQK2PQ8rLp/6EPuaqbgG+pqZrs1s6MoIRIC8B8iMhc3ZfQe8IX4hmOMMVEkAiVjXJv60cD29maXIJq9O683L4HVDwb2jxofSBAzL3dJJoISIiGo6tXxjsEYY+KuqAYaz3dtSOfuwPKlO1e68xNvP+IulU3FhGCMMeYICipg0jmuDenaE5WFhCwhGGNMsskvi8qPTc37r40xxoTNEoIxxhgghjemRYOItAKbR/jxCtzNcOnE+pwerM/p4Vj6PE5VKw/dmNQJ4ViIyPLD3amXyqzP6cH6nB6i0WebMjLGGANYQjDGGONJ54SwKN4BxIH1OT1Yn9NDxPuctucQjDHGDJfOIwRjjDFBLCEYY4wB0iAhiMj5ItIkIhtE5MbD7BcR+Ym3f5WIzItHnJEUQp8/7fV1lYi8IiJz4hFnJB2tz0HvO0FEBkXkiljGF2mh9FdEFojIChF5S0ReiHWMkRbC33WJiPxRRFZ6fb42HnFGkojcIyItInLYJQEifvxS1ZRtQAawEZgAZAMrgemHvOdC4ElAgJOBpfGOOwZ9PgUY5T2/IB36HPS+vwBPAFfEO+4o/xuXAm8D9d7rqnjHHYM+fwP4gfe8EtgDZMc79mPs94eBecCaI+yP6PEr1UcIJwIbVHWTqvYB9wOXHPKeS4B71XkVKBWR2lgHGkFH7bOqvqKBNatfBcbEOMZIC+XfGeDLwMNASyyDi4JQ+nsV8HtV3QKgqunQZwWKRESAQlxCGIhtmJGlqi/i+nEkET1+pXpCGA1sDXq9zdsW7nuSSbj9uQ73DSOZHbXPIjIatxrfnTGMK1pC+TeeAowSkcUi8rqIXBOz6KIjlD7fBkwDdgCrga+oqj824cVNRI9fqV7+Wg6z7dDrbEN5TzIJuT8iciYuIZwW1YiiL5Q+3wp8XVUH3RfIpBZKfzOB44GzgTxgiYi8qqrroh1clITS548AK4CzgInAsyLyV1U9EO3g4iiix69UTwjbgLFBr8fgvj2E+55kElJ/RGQ2cDdwgaq2xSi2aAmlz/OB+71kUAFcKCIDqvpIbEKMqFD/rneraifQKSIvAnOAZE0IofT5WuAWdZPrG0TkXWAq8FpsQoyLiB6/Un3KaBkwWUTGi0g28CngsUPe8xhwjXe2/mRgv6rujHWgEXTUPotIPfB74Ook/sYY7Kh9VtXxqtqgqg3AQ8D1SZoMILS/60eB00UkU0TygZOAd2IcZySF0uctuBERIlINNAKbYhpl7EX0+JXSIwRVHRCRG4CncVcp3KOqb4nIF739d+KuOLkQ2AB04b5lJK0Q+/wtoBy4w/vGPKBJXCkyxD6njFD6q6rviMhTwCrAD9ytqoe9dDEZhPhv/F3gFyKyGjeV8nVVTeqS2CJyH7AAqBCRbcC3gSyIzvHLSlcYY4wBUn/KyBhjTIgsIRhjjAEsIRhjjPFYQjDGGANYQjDGGOOxhGBMhIlIqYhcH+84jAmXJQRjIq8UsIRgko4lBGMi7xZgorcWwQ/jHYwxobIb04yJMBFpAB5X1ZlxDsWYsNgIwRhjDGAJwRhjjMcSgjGR1w4UxTsIY8JlCcGYCPPWl3hZRNbYSWWTTOyksjHGGMBGCMYYYzyWEIwxxgCWEIwxxngsIRhjjAEsIRhjjPFYQjDGGANYQjDGGOP5/y/pzSr9ups7AAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 2 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "N = 100\n",
    "tspan = list(np.linspace(t0, tf, N+1))\n",
    "xf, pf = f.val(t0, x0, p0_sol, tspan, lbda)\n",
    "\n",
    "fig = plt.figure()\n",
    "ax1 = fig.add_subplot(211)\n",
    "ax1.plot(tspan, xf)\n",
    "ax1.set_xlabel('t')\n",
    "ax1.set_ylabel('$x_1$ and $x_2$')\n",
    "ax1.axhline(0, color='k')\n",
    "ax1.axvline(0, color='k')\n",
    "\n",
    "ax2 = fig.add_subplot(212)\n",
    "ax2.plot(tspan, pf)\n",
    "ax2.set_xlabel('t')\n",
    "ax2.set_ylabel('$p_1$ and $p_2$')\n",
    "ax2.axhline(0, color='k')\n",
    "ax2.axvline(0, color='k')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
