#!/usr/bin/env python3

import os
import sys
#import pkg_resources

#sys.path.insert(0, os.path.abspath('../../src'))

# Use sphinx-quickstart to create your own conf.py file!
# After that, you have to edit a few things.  See below.

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# Select nbsphinx and, if needed, other Sphinx extensions:
extensions = [
    'matplotlib.sphinxext.plot_directive',
    'sphinx.ext.autodoc',   # for generation of api documentation
    'sphinx.ext.napoleon',  # numpy docstrings
    'sphinx.ext.intersphinx', # to reference others projects, as numpy.ndarray
#    'sphinx.ext.doctest',   # for the presentation of result in api documentation examples
    'recommonmark',         # for the use of markdown files for the documentation
    'nbsphinx',
    'nbsphinx_link', # A sphinx extension for including notebook files from outside the sphinx source root.
    'sphinx_copybutton',  # for "copy to clipboard" buttons
    'sphinx.ext.mathjax',  # for math equations
    'sphinx.ext.viewcode',
    'sphinxcontrib.bibtex',  # for bibliographic references
#    'sphinxcontrib.rsvgconverter',  # for SVG->PDF conversion in LaTeX output
    'sphinx_gallery.load_style',  # load CSS for gallery (needs SG >= 0.6)
]

## Napoleon settings
napoleon_google_docstring = False
napoleon_numpy_docstring = True
#napoleon_include_init_with_doc = False
#napoleon_include_private_with_doc = False
#napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = True
napoleon_use_ivar = True
napoleon_use_param = False
napoleon_use_rtype = False
#napoleon_use_keyword = True

#intersphinx_mapping = {
#    'python': ('https://docs.python.org/', None),
#    'numpy': ('http://docs.scipy.org/doc/numpy/', None)
#}
intersphinx_mapping = {'python': ('http://docs.python.org/', None),
                       'numpy': ('http://docs.scipy.org/doc/numpy', None),
                       'scipy': ('http://docs.scipy.org/doc/scipy/reference', None),
                       'matplotlib': ('http://matplotlib.sourceforge.net', None)}

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# Exclude build directory and Jupyter backup files:
exclude_patterns = ['_build', '**.ipynb_checkpoints', 'Thumbs.db', '.DS_Store']

# Default language for syntax highlighting in reST and Markdown cells:
highlight_language = 'none'

# Don't add .txt suffix to source files:
#html_sourcelink_suffix = ''

# Work-around until https://github.com/sphinx-doc/sphinx/issues/4229 is solved:
#html_scaled_image_link = False

# List of arguments to be passed to the kernel that executes the notebooks:
# If you use Matplotlib for plots, this setting is recommended:
nbsphinx_execute_arguments = [
    "--InlineBackend.figure_formats={'svg', 'pdf'}",
    "--InlineBackend.rc={'figure.dpi': 96}",
]

nbsphinx_execute = 'never'

# To get a prompt similar to the Classic Notebook, use
nbsphinx_input_prompt = ' In [%s]:'
nbsphinx_output_prompt = ' Out [%s]:'
#nbsphinx_prompt_width = '10'

# Use a different kernel than stored in the notebook metadata, e.g.:
# nbsphinx_kernel_name = 'python3'

# Environment variables to be passed to the kernel:
#os.environ['MY_DUMMY_VARIABLE'] = 'Hello from conf.py!'

#nbsphinx_thumbnails = {
#    'gallery/thumbnail-from-conf-py': 'gallery/a-local-file.png',
#    'gallery/*-rst': '_static/copy-button.svg',
#}

# --------------------------------------------
# --------------------------------------------
# Debut : Cette partie ne fonctionne pas
# --------------------------------------------
# --------------------------------------------
## This is processed by Jinja2 and inserted before each notebook
#nbsphinx_prolog = """
#{% set docname = env.doc2path(env.docname, base=None) %}
#
#.. only:: html
#
#    Mettre ici un lien vers binder, etc. Pour l'instant, je ne sais pas comment faire.
#    Il faut peut-être que le projet soit public.
#    On peut comme ici mettre un texte automatique pour tous les notebooks. Voir https://github.com/spatialaudio/nbsphinx/tree/0.6.0.
#    On peut aussi mettre le lien binder dans le notebook directement.
#
#.. only:: latex
#
#    The following section was created from :file:`{{ docname }}`.
#"""


# This is processed by Jinja2 and inserted before each notebook
nbsphinx_prolog = r"""
{% set docname = 'examples/' + env.doc2path(env.docname, base=None) %}

.. raw:: html

    <div class="admonition note">
      <p>Notebook source code:
        <a class="reference external" href="https://gitlab.inria.fr/ct/gallery/-/blob/master/{{ docname|e }}">{{ docname|e }}</a>
        <br>Run it yourself on binder
        <a href="https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery.git/master?urlpath=lab/tree/{{ docname|e }}"><img alt="Binder badge" src="https://mybinder.org/badge_logo.svg" style="vertical-align:text-bottom"></a>
      </p>
    </div>

.. raw:: latex

    \nbsphinxstartnotebook{\scriptsize\noindent\strut
    \textcolor{gray}{The following section was generated from
    \sphinxcode{\sphinxupquote{\strut {{ docname | escape_latex }}}} \dotfill}}
"""


#      <script>
#        if (document.location.host) {
#          var p = document.currentScript.previousSibling.previousSibling;
#          var a = document.createElement('a');
#          a.innerHTML = 'View in <em>nbviewer</em>';
#          a.href = `https://nbviewer.jupyter.org/url${
#            (window.location.protocol == 'https:' ? 's/' : '/') +
#            window.location.host +
#            window.location.pathname.slice(0, -4) }ipynb`;
#          a.classList.add('reference');
#          a.classList.add('external');
#          p.appendChild(a);
#          p.appendChild(document.createTextNode('.'));
#        }
#      </script>

#nbsphinx_prolog = """
#----
#
#Generated by nbsphinx_ from a Jupyter_ notebook.
#
#.. _nbsphinx: https://nbsphinx.readthedocs.io/
#.. _Jupyter: https://jupyter.org/
#"""
#
#nbsphinx_prolog = """
#Go there: https://example.org/notebooks/{{ env.doc2path(env.docname, base=None) }}
#
#----
#"""


#
## This is processed by Jinja2 and inserted after each notebook
#nbsphinx_epilog = r"""
#{% set docname = 'doc/' + env.doc2path(env.docname, base=None) %}
#.. raw:: latex
#    \nbsphinxstopnotebook{\scriptsize\noindent\strut
#    \textcolor{gray}{\dotfill\ \sphinxcode{\sphinxupquote{\strut
#    {{ docname | escape_latex }}}} ends here.}}
#"""
# --------------------------------------------
# --------------------------------------------
# Fin : Cette partie ne fonctionne pas
# --------------------------------------------
# --------------------------------------------

#mathjax_config = {
#    'TeX': {'equationNumbers': {'autoNumber': 'AMS', 'useLabelIds': True}},
#}

# Additional files needed for generating LaTeX/PDF output:
#latex_additional_files = ['references.bib']

# Support for notebook formats other than .ipynb
#nbsphinx_custom_formats = {
#    '.pct.py': ['jupytext.reads', {'fmt': 'py:percent'}],
#}

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------
# -- The settings below this line are not specific to nbsphinx ------------
#

# where we stop the documentation: when coming to fortran files
#autodoc_mock_imports = ['nutopy.mod_nlesolve',
#                        'nutopy.mod_ivpsolve',
#                        'nutopy.mod_pathsolve',
#                        ]

# Default processing flags for sphinx
autoclass_content = 'class'
autodoc_member_order = 'bysource'
autodoc_default_options = {
    'members': True,
    'undoc-members':True,
    'show-inheritance':True
}

#autodoc_member_order = 'alphabetical' #'bysource'
add_module_names = False
add_function_parentheses = False

# The suffix(es) of source filenames.
source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

#---sphinx-themes-----
html_theme = 'sphinx_rtd_theme'

# options for sphinx_rtd_theme: see https://sphinx-rtd-theme.readthedocs.io/en/stable/configuring.html#
html_theme_options = {
    'canonical_url': '',
#    'analytics_id': 'UA-XXXXXXX-1',  #  Provided by Google in your dashboard
    'logo_only': False,
    'display_version': False,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': False,
 #   'style_nav_header_background': 'white',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': True,
    'titles_only': False
}

author      = 'Olivier Cots'
project     = u'control toolbox gallery'
import time
copyright   =  u'%s, ct gallery project.' % time.strftime('%Y') # a changer
#version     = '0.2' # a voir comment gerer doc et version
#release     = '0.2.1'
html_logo   = 'logo-ct.png'

master_doc = 'index'

# Grab the setup entry
#distribution = pkg_resources.require(project)[0]


#linkcheck_ignore = [r'http://localhost:\d+/']

# -- Get version information and date from Git ----------------------------

try:
    from subprocess import check_output
    release = check_output(['git', 'describe', '--tags', '--always'])
    release = release.decode().strip()
    today = check_output(['git', 'show', '-s', '--format=%ad', '--date=short'])
    today = today.decode().strip()
except Exception:
    release = '<unknown>'
    today = '<unknown date>'

# -- Options for HTML output ----------------------------------------------

html_title = project #+ ' version ' + release

# -- Options for LaTeX output ---------------------------------------------

# See https://www.sphinx-doc.org/en/master/latex.html
#latex_elements = {
#    'papersize': 'a4paper',
#    'printindex': '',
#    'sphinxsetup': r"""
#        %verbatimwithframe=false,
#        %verbatimwrapslines=false,
#        %verbatimhintsturnover=false,
#        VerbatimColor={HTML}{F5F5F5},
#        VerbatimBorderColor={HTML}{E0E0E0},
#        noteBorderColor={HTML}{E0E0E0},
#        noteborder=1.5pt,
#        warningBorderColor={HTML}{E0E0E0},
#        warningborder=1.5pt,
#        warningBgColor={HTML}{FBFBFB},
#    """,
#    'preamble': r"""
#\usepackage[sc,osf]{mathpazo}
#\linespread{1.05}  % see http://www.tug.dk/FontCatalogue/urwpalladio/
#\renewcommand{\sfdefault}{pplj}  % Palatino instead of sans serif
#\IfFileExists{zlmtt.sty}{
#    \usepackage[light,scaled=1.05]{zlmtt}  % light typewriter font from lmodern
#}{
#    \renewcommand{\ttdefault}{lmtt}  % typewriter font from lmodern
#}
#\usepackage{booktabs}  % for Pandas dataframes
#""",
#}
#
#latex_documents = [
#    (master_doc, 'nbsphinx.tex', project, author, 'howto'),
#]
#
#latex_show_urls = 'footnote'
#latex_show_pagerefs = True

# -- Options for EPUB output ----------------------------------------------

# These are just defined to avoid Sphinx warnings related to EPUB:
version = release
#suppress_warnings = ['epub.unknown_project_files']


