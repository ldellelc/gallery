      subroutine hfun(t, x, p, tmax, mass0, beta, mu, h, n)

      integer          n

      double precision t
      double precision x(n)
      double precision p(n)
      double precision h
      double precision tmax
      double precision beta
      double precision mass0
      double precision mu

      double precision pa
      double precision ex
      double precision ey
      double precision hx
      double precision hy
      double precision lg
      double precision p1
      double precision p2
      double precision p3
      double precision p4
      double precision p5
      double precision p6
      double precision pdm
      double precision cl
      double precision sl
      double precision w
      double precision pdmw
      double precision zz
      double precision uh
      double precision f06
      double precision f12
      double precision f13
      double precision f21
      double precision f22
      double precision f23
      double precision f32
      double precision f33
      double precision f34
      double precision f35
      double precision f36
      double precision h0
      double precision h1
      double precision h2
      double precision h3
      double precision mass
      double precision psi

      pa = x(1)
      ex = x(2)
      ey = x(3)
      hx = x(4)
      hy = x(5)
      lg = x(6)

      p1 = p(1)
      p2 = p(2)
      p3 = p(3)
      p4 = p(4)
      p5 = p(5)
      p6 = p(6)

      pdm = sqrt(pa/mu) 
      cl = cos(lg)
      sl = sin(lg)
      w = 1.0+ex*cl+ey*sl
      pdmw = pdm/w 
      zz = hx*sl-hy*cl
      uh = (1+hx**2+hy**2)/2.0

      f06 = w**2/(pa*pdm)
      h0  = p6*f06

      f12 = pdm *   sl
      f13 = pdm * (-cl)
      h1  = p2*f12 + p3*f13

      f21 = pdm * 2.0 * pa / w
      f22 = pdm * (cl+(ex+cl)/w)
      f23 = pdm * (sl+(ey+sl)/w)
      h2  = p1*f21 + p2*f22 + p3*f23

      f32 = pdmw * (-zz*ey)
      f33 = pdmw * zz*ex
      f34 = pdmw * uh * cl
      f35 = pdmw * uh * sl
      f36 = pdmw * zz
      h3  = p2*f32 + p3*f33 + p4*f34 + p5*f35 + p6*f36

      mass = mass0 - beta*tmax*t

      psi = sqrt(h1**2 + h2**2 + h3**2)

      h = -1.0 + h0 + (tmax/mass) * psi
       
      return
      end
