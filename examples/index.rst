Welcome to ct gallery
=====================

Examples of notebooks from the ct (control toolbox) project
using nutopy package and others.

.. nbgallery::
    :caption: Optimal Control Problems
    :name: ocp-gallery
    :glob:
    :reversed:

    ihm/ihm
    kepler-c/kepler-c
    kepler/kepler
    goddard2/goddard2_bocop
    smooth_case/smooth_case

.. nbgallery::
    :caption: Initial Value Problems
    :name: ivp-gallery
    :glob:
    :reversed:

    sir/SIR
