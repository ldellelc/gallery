# ct (control toolbox) gallery

Jupyter notebooks for use cases using solvers of the nutopy package and others.

[![Online examples](https://img.shields.io/badge/ct-gallery-blue.svg)](https://ct.gitlabpages.inria.fr/gallery)
[![Full directory](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Fct%2Fgallery.git/master?urlpath=lab/)


